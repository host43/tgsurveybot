package users

import (
	"fmt"
	"sync"
	"time"
)

type User struct {
	Id        int64  `json:"uid"`
	TgName    string `json:"tgname"`
	Sid       int
	Timestamp time.Time
	Attrs     map[string]interface{} `json:"attrs,omitempty"`
}

type Users struct {
	Users map[int64]*User
	mux   sync.RWMutex
}

func New() *Users {
	users := &Users{Users: make(map[int64]*User)}
	return users
}

func (u *Users) NewUser(id int64, tgName string, sid int) {
	u.mux.Lock()
	defer u.mux.Unlock()
	u.Users[id] = &User{
		id,
		tgName,
		sid,
		time.Now(),
		map[string]interface{}{},
	}
}

func (u *Users) GetUser(id int64) *User {
	u.mux.RLock()
	defer u.mux.RUnlock()
	return u.Users[id]
}

func (u *Users) GetIdentity(id int64) string {
	u.mux.RLock()
	defer u.mux.RUnlock()
	tgname := u.Users[id].TgName
	if name, ok := u.GetAttr(id, "name"); ok {
		if name, ok := name.(string); ok {
			return fmt.Sprintf("%d_@%s_%s", id, tgname, name)
		}
	}
	return fmt.Sprintf("%d_@%s_undefined", id, tgname)
}

func (u *Users) SetAttr(id int64, attr string, val interface{}) {
	u.mux.Lock()
	defer u.mux.Unlock()
	u.Users[id].Attrs[attr] = val
}

func (u *Users) GetAttr(id int64, attr string) (interface{}, bool) {
	u.mux.RLock()
	defer u.mux.RUnlock()
	val, ok := u.Users[id].Attrs[attr]
	return val, ok
}

func (u *Users) GetSID(id int64) int {
	u.mux.RLock()
	defer u.mux.RUnlock()
	return u.Users[id].Sid
}

func (u *Users) SetSID(id int64, sid int) {
	u.mux.Lock()
	defer u.mux.Unlock()
	u.Users[id].Sid = sid
}

func (u *Users) SetTimestamp(id int64) {
	u.mux.Lock()
	defer u.mux.Unlock()
	u.Users[id].Timestamp = time.Now()
}

func (u *Users) GetTimestamp(id int64) time.Time {
	u.mux.RLock()
	defer u.mux.RUnlock()
	return u.Users[id].Timestamp
}

func (u *Users) GetAllUIDs() []int64 {
	u.mux.RLock()
	defer u.mux.RUnlock()
	uids := []int64{}
	for id, _ := range u.Users {
		uids = append(uids, id)
	}
	return uids
}

func (u *Users) Delete(id int64) *User {
	user := u.Users[id]
	delete(u.Users, id)
	return user
}

func (u *Users) IsExists(id int64) bool {
	_, ok := u.Users[id]
	return ok
}
