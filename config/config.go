package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type Proxy struct {
	proto    string `json:"proto"`
	host     string `json:"host"`
	port     string `json:"port"`
	username string `json:"username"`
	password string `json:"password"`
}

type Config struct {
	Token    string  `json:"token"`
	AdminIds []int64 `json:"admin_ids"`
	StoreDst string  `json:"store_to"`
	Timeout  int     `json:"user_timeout"`
	LastStep int     `json:"last_step"`
	Proxy    *Proxy  `json:"proxy,omitempty"`
}

func LoadConfig(f *os.File) (*Config, error) {
	jsonData, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	config := &Config{}
	err = json.Unmarshal(jsonData, config)
	if err != nil {
		return nil, err
	}
	return config, nil
}
